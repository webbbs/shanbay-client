from sqlalchemy import Column, Integer, String
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class Word(Base):
    __tablename__ = 'word'

    word = Column(String, primary_key=True)
    trans = Column(String)
    phonetic = Column(String)
    shanbay_id = Column(Integer)
    learning_id = Column(Integer)

    def __repr__(self):
        return u"<Word(word='%s', trans='%s')>" % (self.word, self.trans)
