
# 扇贝客户端

1. 安装依赖

```bash
pip install shanbay bs4 sqlalchemy alembic lxml
```

2. 创建数据库脚本

```bash
alembic upgrade head
```

3. 获取 access_token
 
打开页面: 

`https://api.shanbay.com/oauth2/authorize/?response_type=token&client_id=fea77d2c655322d903db&redirect_uri=https%3A%2F%2Fapi.shanbay.com%2Foauth2%2Fauth%2Fsuccess%2F&state=OqrhceLuWqpREUi8Snx5sb4dIAykfG`
