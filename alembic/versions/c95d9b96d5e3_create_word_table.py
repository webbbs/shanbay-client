"""create word table

Revision ID: c95d9b96d5e3
Revises: 
Create Date: 2017-01-21 16:22:33.857076

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'c95d9b96d5e3'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'word',
        sa.Column('word', sa.String, primary_key=True),
        sa.Column('trans', sa.String),
        sa.Column('phonetic', sa.String),
        sa.Column('learning_id', sa.Integer),
    )

def downgrade():
    pass
