"""add shanbay word id

Revision ID: 77e3d4229467
Revises: c95d9b96d5e3
Create Date: 2017-01-21 17:44:47.939830

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '77e3d4229467'
down_revision = 'c95d9b96d5e3'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column('word', sa.Column('shanbay_id', sa.Integer))

def downgrade():
    pass
