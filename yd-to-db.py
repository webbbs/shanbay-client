#!/usr/bin/env python
# coding:utf-8

# 将有道单词本单词导入数据库

import sys

import bs4
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from models.word import Word

engine = create_engine('sqlite:///words.db', echo=True)
Session = sessionmaker(bind=engine)


def parse_yd():
    xml = bs4.BeautifulSoup(open(sys.argv[1]), 'xml')
    return xml.findAll('item')


def main():
    words = parse_yd()
    session = Session()
    for word in words:
        word = Word(word=word.word.text, trans=word.trans.text, phonetic=word.phonetic.text)
        session.merge(word)

    session.commit()


if __name__ == '__main__':
    main()
