# coding:utf-8

from __builtin__ import object, super
from exceptions import Exception

import shanbay


class ShanbayService(object):
    """
    ShanbayService
    """

    def __init__(self, client_id, access_token):
        super(ShanbayService, self).__init__()
        self.client_id = client_id
        self.access_token = access_token
        self.api = shanbay.API(self.client_id, {'access_token': self.access_token})

    def add_word(self, word):
        """
        :param word:
        :return: [shanbay_id, learning_id]
        """
        print "importing %s to shanbay..." % (word)

        sb_word = self.api.word(word)
        # print json.dumps(sb_word)

        if sb_word['status_code'] == 1:
            return [-1, None]

        if sb_word['status_code'] == 0:
            shanbay_id = sb_word['data']['id']
            if 'learning_id' in sb_word['data']:
                return [shanbay_id, sb_word['data']['learning_id']]
            else:
                learning_id = self.__add_to_sb(shanbay_id)
                return [shanbay_id, learning_id]

    def __add_to_sb(self, word_id):
        try:
            # 添加失败可以忽略
            add_result = self.api.add_word(word_id)
            if add_result['status_code'] == 0:
                return add_result['data']['id']
        except Exception as e:
            print e

        return None
