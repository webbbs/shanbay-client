import sqlalchemy as sa
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from models.word import Word

engine = create_engine('sqlite:///words.db', echo=True)
Session = sessionmaker(bind=engine)


class WordService(object):
    """docstring for WordService."""

    def __init__(self):
        super(WordService, self).__init__()
        self.session = Session()

    def select_word(self):
        for word in self.session.query(Word) \
                        .filter(Word.learning_id.is_(None)) \
                        .filter(sa.or_(Word.shanbay_id != '-1', Word.shanbay_id.is_(None))) \
                        .order_by(Word.word)[0:1]: \
            return word

    def save_word(self, word):
        self.session.merge(word)
        self.session.commit()
