#!/usr/bin/env python
# coding:utf-8

# 将单词导入扇贝
# http://www.shanbay.com/developer/wiki/api_v1/
from __builtin__ import xrange

from services.shanbay_service import ShanbayService
from services.word_service import WordService

client_id = 'fea77d2c655322d903db'
access_token = 'fklXk1kKTsCJ0YaD7j98ue3pBbdunT'

BATCH = 20


def main():
    word_service = WordService()
    shanbay_service = ShanbayService(client_id, access_token)

    for i in xrange(0, BATCH):
        word = word_service.select_word()
        if not word:
            return

        word_id, learning_id = shanbay_service.add_word(word.word)
        word.shanbay_id = word_id
        word.learning_id = learning_id
        word_service.save_word(word)


if __name__ == '__main__':
    main()
